# Deeplab video processing time
This document contains video processing speed indicators using opencv and pre-trained deeplab models on dataset ade20k :
- Mobilenetv2
- Xception65


| **ADE20k** | **Resize**  | | | |
| ------ | ------ |------|------|------|
| **NN \ frames** | 1 | 1 + visual(plt) | 300 | **frame gluging**|
| **mobilenetv2** | 1,56 | 2,54 | 7m 15|1,09|
| **xception65** | 11,06 | 12,07 | 56m 59 |1,09|
|  | **crop** |
| **mobilenetv2** |1,24 |   | 3m 3 |  | 1,09|
### DNN time for 1 frame
![time 1](/dnn-pictures/1_kadr.png "Time for 1 frame")
### DNN time for 300 frames
![time 2](/dnn-pictures/300_kadrov.png "Time for 300 frames")
